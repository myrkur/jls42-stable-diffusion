#!/bin/bash

# Emplacement des fichiers
ENV_FILE="terraform/.env"
GITLAB_CI_FILE=".gitlab-ci.yml"

# Vérifie si le fichier .env existe déjà
if [[ -e $ENV_FILE ]]; then
  echo "Le fichier $ENV_FILE existe déjà. Veuillez le supprimer ou le renommer avant d'exécuter ce script."
  exit 1
fi

# Demande à l'utilisateur de fournir des valeurs ou d'utiliser des valeurs par défaut
read -p "Nom du bucket (par défaut: Donner_un_nom_a_votre_bucket): " STATE_BUCKET_NAME
STATE_BUCKET_NAME="${STATE_BUCKET_NAME:-Donner_un_nom_a_votre_bucket}"

read -p "Chemin de la clé publique SSH (par défaut: ${HOME}/.ssh/id_rsa.pub): " SSH_PUBLIC_KEY_PATH
SSH_PUBLIC_KEY_PATH="${SSH_PUBLIC_KEY_PATH:-${HOME}/.ssh/id_rsa.pub}"

read -p "Répertoire distant (par défaut: /home/ubuntu/stable-diffusion-webui/log/images/): " REMOTE_DIRECTORY
REMOTE_DIRECTORY="${REMOTE_DIRECTORY:-/home/ubuntu/stable-diffusion-webui/log/images/}"

read -p "Répertoire local (par défaut: ${HOME}/sd_output/): " LOCAL_DIRECTORY
LOCAL_DIRECTORY="${LOCAL_DIRECTORY:-${HOME}/sd_output/}"

read -p "Supprimer les fichiers source ? (true/false, par défaut: true): " REMOVE_SOURCE_FILES
REMOVE_SOURCE_FILES="${REMOVE_SOURCE_FILES:-true}"

read -p "Uniquement les fichiers ZIP ? (true/false, par défaut: false): " ONLY_ZIP_FILES
ONLY_ZIP_FILES="${ONLY_ZIP_FILES:-false}"

read -p "Port du tunnel SSH (par défaut: 8142): " SSH_TUNNEL_PORT
SSH_TUNNEL_PORT="${SSH_TUNNEL_PORT:-8142}"

read -p "Port de redirection locale (par défaut: 7860): " LOCAL_FORWARDING_PORT
LOCAL_FORWARDING_PORT="${LOCAL_FORWARDING_PORT:-7860}"

read -p "Région AWS (par défaut: eu-west-1): " AWS_DEFAULT_REGION
AWS_DEFAULT_REGION="${AWS_DEFAULT_REGION:-eu-west-1}"

read -p "Votre email (par exemple: example@example.com): " SUBSCRIBER_EMAIL
SUBSCRIBER_EMAIL="${SUBSCRIBER_EMAIL:-example@example.com}"

# Obtenir l'IP publique actuelle de l'utilisateur
CURRENT_IP=$(curl -s ipinfo.io/ip)
read -p "IPs autorisées (par défaut: ${CURRENT_IP}/32): " ALLOWED_IP
ALLOWED_IP="${ALLOWED_IP:-${CURRENT_IP}/32}"

# Génère le fichier terraform.tfvars
cat <<EOL > terraform/terraform.tfvars
region             = "${AWS_DEFAULT_REGION}"
vpc_cidr           = "10.0.0.0/24"
subnet_cidrs       = ["10.0.0.0/26", "10.0.0.64/26", "10.0.0.128/26"]
availability_zones = ["${AWS_DEFAULT_REGION}a", "${AWS_DEFAULT_REGION}b", "${AWS_DEFAULT_REGION}c"]
instance_type      = "g4dn.xlarge"
spot_max_price     = {
  "g4dn.2xlarge" = "0.388300"
  "g4dn.xlarge"  = "0.197300"
}
autostop_time              = "00:00"
budget_alert_thresholds    = [10, 20, 42, 50]
budget_name                = "budget-global-monthly"
budget_type                = "COST"
limit_unit                 = "USD"
time_unit                  = "MONTHLY"
subscriber_email_addresses = ["${SUBSCRIBER_EMAIL}"]
volume_size                = 100
allowed_ips                = ["${ALLOWED_IP}"]
EOL

echo "Fichier terraform/terraform.tfvars généré."

# Génère le fichier .env
cat <<EOL > $ENV_FILE
# Variables globales
STATE_BUCKET_NAME="$STATE_BUCKET_NAME"
SSH_PUBLIC_KEY_PATH="$SSH_PUBLIC_KEY_PATH"
REMOTE_DIRECTORY="$REMOTE_DIRECTORY"
LOCAL_DIRECTORY="$LOCAL_DIRECTORY"
REMOVE_SOURCE_FILES=$REMOVE_SOURCE_FILES
ONLY_ZIP_FILES=$ONLY_ZIP_FILES
AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION"

# Ports
SSH_TUNNEL_PORT=$SSH_TUNNEL_PORT
LOCAL_FORWARDING_PORT=$LOCAL_FORWARDING_PORT
EOL

echo "Fichier $ENV_FILE généré."

# Modifie le fichier .gitlab-ci.yml
if [[ -e $GITLAB_CI_FILE ]]; then
  sed -i "s/# STATE_BUCKET_NAME:.*/STATE_BUCKET_NAME: $STATE_BUCKET_NAME     ## Pour sauvegarder l'état (state) de votre Terraform/" $GITLAB_CI_FILE
  sed -i "s/AWS_DEFAULT_REGION:.*/AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION/" $GITLAB_CI_FILE
  echo "Valeurs de STATE_BUCKET_NAME et AWS_DEFAULT_REGION mises à jour et décommentées dans $GITLAB_CI_FILE."
else
  echo "Le fichier $GITLAB_CI_FILE n'a pas été trouvé. Aucune modification n'a été apportée à ce fichier."
fi
