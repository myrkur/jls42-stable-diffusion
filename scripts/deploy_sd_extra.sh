#!/bin/bash

# il y a un modèle déployé mais c'est v1-5-pruned-emaonly.safetensors 
# Déploiement du modèle sdxl base et son refiner
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0/resolve/main/sd_xl_base_1.0.safetensors" -d stable-diffusion-webui/models/Stable-diffusion -o sd_xl_base_1.0.safetensors
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "https://huggingface.co/stabilityai/stable-diffusion-xl-refiner-1.0/resolve/main/sd_xl_refiner_1.0.safetensors" -d stable-diffusion-webui/models/Stable-diffusion -o sd_xl_refiner_1.0.safetensors

# installation de l'extension controlnet
git clone https://github.com/Mikubill/sd-webui-controlnet stable-diffusion-webui/extensions/sd-webui-controlnet

# deploiement des modèles XL pour controlnet
BASE_URL="https://huggingface.co/lllyasviel/sd_control_collection/resolve/main/"

MODELS=(
    "diffusers_xl_canny_full.safetensors"
    "diffusers_xl_depth_full.safetensors"
    "ioclab_sd15_recolor.safetensors"
    "ip-adapter_sd15_plus.pth"
    "ip-adapter_sd15.pth"
    "ip-adapter_xl.pth"
    "kohya_controllllite_xl_blur.safetensors"
    "kohya_controllllite_xl_blur_anime.safetensors"
    "kohya_controllllite_xl_canny.safetensors"
    "kohya_controllllite_xl_canny_anime.safetensors"
    "kohya_controllllite_xl_blur_anime_beta.safetensors"
    "kohya_controllllite_xl_depth.safetensors"
    "kohya_controllllite_xl_depth_anime.safetensors"
    "kohya_controllllite_xl_openpose_anime.safetensors"
    "kohya_controllllite_xl_openpose_anime_v2.safetensors"
    "kohya_controllllite_xl_scribble_anime.safetensors"
    "sai_xl_canny_256lora.safetensors"
    "sai_xl_depth_256lora.safetensors"
    "sai_xl_recolor_256lora.safetensors"
    "sai_xl_sketch_256lora.safetensors"
    "sargezt_xl_depth.safetensors"
    "sargezt_xl_depth_faid_vidit.safetensors"
    "sargezt_xl_depth_zeed.safetensors"
    "sargezt_xl_softedge.safetensors"
    "t2i-adapter_diffusers_xl_canny.safetensors"
    "t2i-adapter_diffusers_xl_depth_midas.safetensors"
    "t2i-adapter_diffusers_xl_depth_zoe.safetensors"
    "t2i-adapter_diffusers_xl_lineart.safetensors"
    "t2i-adapter_diffusers_xl_openpose.safetensors"
    "t2i-adapter_xl_canny.safetensors"
    "t2i-adapter_xl_openpose.safetensors"
    "t2i-adapter_xl_sketch.safetensors"
    "thibaud_xl_openpose.safetensors"
    "thibaud_xl_openpose_256lora.safetensors"
)

for model in "${MODELS[@]}"; do
    aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "${BASE_URL}${model}" -d stable-diffusion-webui/extensions/sd-webui-controlnet/models -o "${model}"
done

echo "Téléchargement terminé!"

## Lancement de la web ui
nohup ./webui.sh --xformers > output.log 2>&1 & tail -f output.log
