# IAM Role pour la fonction Lambda
resource "aws_iam_role" "lambda_autostop_role" {
  name = "lambda-autostop-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

# Création du fichier ZIP pour la fonction Lambda
data "archive_file" "autostop_zip" {
  type        = "zip"
  source_file = "${path.module}/lambdas/autostop.py"
  output_path = "${path.module}/autostop_package.zip"
}

# Fonction Lambda pour l'arrêt automatique
resource "aws_lambda_function" "autostop" {
  function_name = "autostop"
  role          = aws_iam_role.lambda_autostop_role.arn
  handler       = "autostop.handler" # Le nom du fichier sans l'extension, suivi de la fonction
  runtime       = "python3.8"        # Le runtime que vous utilisez

  filename = data.archive_file.autostop_zip.output_path # Utilisation du fichier ZIP créé par Terraform
}



# Événement planifié pour invoquer la fonction Lambda
resource "aws_cloudwatch_event_rule" "autostop_schedule" {
  schedule_expression = "cron(0 ${var.autostop_time} * * ? *)"
}

# Association de l'événement planifié avec la fonction Lambda
resource "aws_cloudwatch_event_target" "autostop_target" {
  rule = aws_cloudwatch_event_rule.autostop_schedule.name
  arn  = aws_lambda_function.autostop.arn
}

# Politique IAM pour permettre à la fonction Lambda de décrire les instances EC2
resource "aws_iam_policy" "lambda_autostop_policy" {
  name        = "lambda-autostop-policy"
  description = "Permet à la fonction Lambda de décrire les instances EC2"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "ec2:DescribeInstances",
          "ec2:StopInstances" # Si vous souhaitez également arrêter les instances
        ],
        Effect   = "Allow",
        Resource = "*"
      }
    ]
  })
}

# Attacher la politique IAM au rôle Lambda
resource "aws_iam_role_policy_attachment" "lambda_autostop_policy_attachment" {
  role       = aws_iam_role.lambda_autostop_role.name
  policy_arn = aws_iam_policy.lambda_autostop_policy.arn
}
