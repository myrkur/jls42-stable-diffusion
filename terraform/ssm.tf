resource "aws_ssm_parameter" "instance_id" {
  name  = "/ec2/sd_instance_id"
  type  = "String"
  value = aws_instance.main.id
}