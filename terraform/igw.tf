# Création d'une Internet Gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id # Attachement à notre VPC

  tags = {
    Name = "main-internet-gateway"
  }
}
