import boto3

def handler(event, context):
    ec2 = boto3.client('ec2')

    # Récupérer toutes les instances EC2 avec le tag 'autostop'
    instances = ec2.describe_instances(Filters=[
        {
            'Name': 'tag-key',
            'Values': ['autostop']
        }
    ])['Reservations']

    for instance in instances:
        for inst in instance['Instances']:
            instance_id = inst['InstanceId']
            tags = inst['Tags']

            # Trouver la valeur du tag 'autostop'
            autostop_value = next((tag['Value'] for tag in tags if tag['Key'] == 'autostop'), 'false')

            # Si la valeur du tag 'autostop' est 'true', éteindre l'instance
            if autostop_value.lower() == 'true':
                print(f"Stopping instance {instance_id}")
                ec2.stop_instances(InstanceIds=[instance_id])
