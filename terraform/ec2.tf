# Récuperation de l'ID de l'AMI
data "aws_ami" "deep_learning" {
  owners      = ["898082745236"]
  most_recent = true

  filter {
    name   = "name"
    values = ["Deep Learning AMI GPU PyTorch*"]
  }

  filter {
    name   = "name"
    values = ["*Ubuntu*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# Création d'une instance EC2
resource "aws_instance" "main" {
  ami                         = data.aws_ami.deep_learning.id
  instance_type               = var.instance_type
  associate_public_ip_address = true
  subnet_id                   = values(aws_subnet.subnet)[var.subnet_index].id
  iam_instance_profile        = aws_iam_instance_profile.ec2_instance_connect_profile.name
  vpc_security_group_ids      = [aws_security_group.sd_inbound.id]
  user_data                   = file("${path.module}/scripts/user_data.sh")
  metadata_options {
    http_tokens = "required"
  }
  root_block_device {
    encrypted   = true
    volume_size = var.volume_size
    volume_type = "gp3"
  }

  # ebs_block_device {
  #   device_name = "/dev/sda1"
  #   volume_size = var.volume_size
  #   volume_type = "gp3"
  #   encrypted   = true
  # }

  instance_market_options {
    market_type = "spot"
    spot_options {
      max_price                      = var.spot_max_price[var.instance_type]
      instance_interruption_behavior = var.spot_request_type == "persistent" ? "stop" : null
      spot_instance_type             = var.spot_request_type
    }
  }

  lifecycle {
    ignore_changes = [ami]
  }

  tags = {
    Name     = "Stable Diffusion"
    autostop = true
  }
}


resource "aws_iam_instance_profile" "ec2_instance_connect_profile" {
  name = "ec2-instance-connect-profile"
  role = aws_iam_role.ec2_instance_connect_role.name
}

resource "aws_security_group" "sd_inbound" {
  name        = "sd-inbound"
  description = "Security group Stable diffusion"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.eic_endpoint.id]
    description     = "Port 22 ouvert en entree filtre par SG"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "L instance peut sortir sur tout internet"
  }

  tags = {
    Name = "sd-inbound"
  }
}

