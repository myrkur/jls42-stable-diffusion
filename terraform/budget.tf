resource "aws_budgets_budget" "budget" {
  name         = var.budget_name
  budget_type  = var.budget_type
  limit_amount = "100" # Vous pouvez définir une limite globale ici
  limit_unit   = var.limit_unit
  time_unit    = var.time_unit

  dynamic "notification" {
    for_each = var.budget_alert_thresholds
    content {
      comparison_operator        = "GREATER_THAN"
      threshold                  = notification.value
      threshold_type             = "PERCENTAGE"
      notification_type          = "ACTUAL"
      subscriber_email_addresses = var.subscriber_email_addresses
    }
  }
}
