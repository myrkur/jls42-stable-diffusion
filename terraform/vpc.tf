resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr # Utilisation de la variable pour le bloc CIDR

  tags = {
    Name = "main-vpc"
  }
}
