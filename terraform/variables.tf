# Region
variable "region" {
  description = "AWS Region"
  type        = string
  default     = "eu-west-1" # Exemple : "eu-west-1" pour l'Irlande
}

# Bloc CIDR pour le VPC
variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/24"
}

# Blocs CIDR pour les subnets
variable "subnet_cidrs" {
  description = "CIDR blocks for the subnets"
  type        = list(string)
  default     = ["10.0.0.0/26", "10.0.0.64/26", "10.0.0.128/26"]
}

# Zones de disponibilité pour les subnets
variable "availability_zones" {
  description = "Availability zones for the subnets"
  type        = list(string)
  default     = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

# Type d'instance EC2
variable "instance_type" {
  description = "Instance type for the EC2 instance"
  type        = string
  default     = "g4dn.xlarge"
}

variable "autostop_time" {
  description = "Time to automatically stop the EC2 instance"
  type        = string
  default     = "00:00"
}

variable "budget_alert_thresholds" {
  description = "List of alert thresholds for the budget"
  type        = list(number)
  default     = [10, 20, 42, 50]
}

variable "budget_name" {
  description = "Name of the budget"
  type        = string
  default     = "budget-global-monthly"
}

variable "budget_type" {
  description = "Type of budget (COST or USAGE)"
  type        = string
  default     = "COST"
}

variable "limit_unit" {
  description = "Measurement unit for the budget"
  type        = string
  default     = "USD"
}

variable "time_unit" {
  description = "Time unit for the budget (e.g., MONTHLY)"
  type        = string
  default     = "MONTHLY"
}

variable "subscriber_email_addresses" {
  description = "List of email addresses to receive budget notifications"
  type        = list(string)
  default     = ["example@example.com"]
}

variable "volume_size" {
  description = "Size of the EBS volume in GB"
  type        = number
  default     = 100
}

variable "allowed_ips" {
  description = "List of IP addresses allowed to access the EC2 Instance Connect endpoint"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "spot_max_price" {
  description = "Prix max pour chaque type d'instance"
  type        = map(string)
  default = {
    "g4dn.2xlarge" = "0.388300"
    "g4dn.xlarge"  = "0.1979"
  }
}

variable "spot_request_type" {
  description = "Type de demande Spot (persistent ou one-time)"
  default     = "persistent"
}

variable "subnet_index" {
  description = "Index du sous-réseau à utiliser [0,1,2]"
  default     = 0
}
