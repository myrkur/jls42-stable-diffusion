# EC2 Instance Connect Endpoint
resource "aws_ec2_instance_connect_endpoint" "instance_connect_endpoint" {
  subnet_id          = values(aws_subnet.subnet)[var.subnet_index].id
  security_group_ids = [aws_security_group.eic_endpoint.id]
}

# IAM Role pour EC2 Instance Connect
resource "aws_iam_role" "ec2_instance_connect_role" {
  name = "ec2-instance-connect-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# IAM Policy pour EC2 Instance Connect
resource "aws_iam_role_policy" "ec2_instance_connect_policy" {
  name = "ec2-instance-connect-policy"
  role = aws_iam_role.ec2_instance_connect_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "ec2-instance-connect:SendSSHPublicKey",
        Resource = "*"
      }
    ]
  })
}

resource "aws_security_group" "eic_endpoint" {
  name        = "eic-endpoint-sg"
  description = "Security group for EC2 Instance Connect Endpoint"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.allowed_ips
    description = "Port 22 ouvert en entree filtre par IP"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Peut sortir sur tout internet"

  }

  tags = {
    Name = "eic-endpoint-sg"
  }
}