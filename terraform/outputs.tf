output "instance_id" {
  description = "L'ID de l'instance EC2"
  value       = aws_instance.main.id
}

# output "instance_public_ip" {
#   description = "L'adresse IP publique de l'instance EC2"
#   value       = aws_instance.main.public_ip
# }
