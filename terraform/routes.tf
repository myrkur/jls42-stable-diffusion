# Création d'une table de routage
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id # Attachement à notre VPC

  route {
    cidr_block = "0.0.0.0/0"                  # Trafic destiné à toutes les adresses IP
    gateway_id = aws_internet_gateway.main.id # Utilisation de notre Internet Gateway
  }

  tags = {
    Name = "main-route-table"
  }
}

# Association de la table de routage à nos subnets
resource "aws_route_table_association" "main" {
  for_each = aws_subnet.subnet

  subnet_id      = each.value.id
  route_table_id = aws_route_table.main.id
}
