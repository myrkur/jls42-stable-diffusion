#!/bin/sh

ACTION=$1

INSTANCE_ID=$(aws ssm get-parameter --name "/ec2/sd_instance_id" --query "Parameter.Value" --output text)

case $ACTION in
  start)
    aws ec2 start-instances --instance-ids $INSTANCE_ID
    ;;
  stop)
    aws ec2 stop-instances --instance-ids $INSTANCE_ID
    ;;
  terminate)
    aws ec2 terminate-instances --instance-ids $INSTANCE_ID
    ;;
  backup)
    AMI_NAME="snapci-$(date +%Y%m%d%H%M%S)"
    aws ec2 create-image --instance-id $INSTANCE_ID --name $AMI_NAME --description "Backup de l'instance EC2 depuis CI" --no-reboot
    ;;
  status)
    STATUS=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[0].Instances[0].State.Name" --output text)
    echo "Statut de l'instance EC2 ($INSTANCE_ID) : $STATUS"
    ;;
  *)
    echo "Action non reconnue: $ACTION"
    exit 1
    ;;
esac
