#!/bin/bash

# Pour voir si les users data sont toujours en cours  ou pas : 
#     sudo cloud-init status
# pour lancer quand c'est pret : while [[ "$(sudo cloud-init status | grep -o 'status: [a-z]*' | cut -d ' ' -f 2)" != "done" ]]; do sleep 10; done && ./webui.sh  --xformers

# Arrêter le script si une commande échoue
set -e

cd /home/ubuntu

# Rediriger la sortie vers un fichier de log pour faciliter le débogage
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Mise à jour des paquets
echo "Mise à jour des paquets..."
sudo apt update && sudo apt upgrade -y


# Vérification et installation des paquets nécessaires
for package in wget git python3.9 python3.9-venv libgl1 libglib2.0-0 libtcmalloc-minimal4 libgoogle-perftools4 ncdu aria2; do
    if ! dpkg -l | grep -q "$package"; then
        echo "Installation de $package..."
        sudo apt install $package -y
    else
        echo "$package est déjà installé."
    fi
done

update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1

# URL du script à télécharger
WEBUI_URL="https://raw.githubusercontent.com/AUTOMATIC1111/stable-diffusion-webui/master/webui.sh"

# Vérification avant téléchargement
if [ ! -f "webui.sh" ]; then
    echo "Téléchargement de webui.sh depuis $WEBUI_URL..."
    wget -q $WEBUI_URL
else
    echo "webui.sh est déjà présent."
fi

# Rendre le script exécutable avec les bons droits
chmod +x webui.sh
chown ubuntu:ubuntu webui.sh

echo "Script user data terminé avec succès!"
