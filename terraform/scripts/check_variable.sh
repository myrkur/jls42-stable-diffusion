#!/bin/sh

check_variable() {
  VAR_VALUE=$(printenv $1)
  if [ -z "$VAR_VALUE" ]; then
    echo "Erreur : la variable $1 n'est pas définie."
    echo "Veuillez renseigner les variables soit dans la section 'variables' du fichier .gitlab-ci.yml, soit dans les variables de CI."
    echo "Pour plus d'informations, consultez : https://docs.gitlab.com/ee/ci/variables/#for-a-project"
    echo "ATTENTION : AWS_SECRET_ACCESS_KEY et AWS_ACCESS_KEY_ID ne devraient JAMAIS être dans un fichier 'gité', mais dans les paramètres de CI."
    exit 1
  fi
}

VARS_TO_CHECK="STATE_BUCKET_NAME AWS_ACCESS_KEY_ID AWS_DEFAULT_REGION AWS_SECRET_ACCESS_KEY"

for VAR in $VARS_TO_CHECK; do
  check_variable "$VAR"
done
