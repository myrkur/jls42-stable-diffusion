
# Utilisation de for_each pour créer les subnets
resource "aws_subnet" "subnet" {
  for_each = { for i, az in var.availability_zones : az => { cidr = var.subnet_cidrs[i], az = az } }

  vpc_id            = aws_vpc.main.id # ID du VPC dans lequel les subnets seront créés
  cidr_block        = each.value.cidr # Bloc CIDR pour chaque subnet
  availability_zone = each.value.az   # Zone de disponibilité pour chaque subnet

  tags = {
    Name = "subnet-${each.key}" # Nom du subnet basé sur la zone de disponibilité
  }
}

