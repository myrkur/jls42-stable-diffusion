#!/bin/bash

# Vérifiez la présence du fichier .env
if [[ ! -f .env ]]; then
  echo "Erreur : Le fichier .env est absent."
  echo
  echo "Créez un fichier .env à la racine avec le contenu suivant :"
  echo "--------------------------------------------------------"
  echo "# Variables globales"
  echo "STATE_BUCKET_NAME=\"monNomdeBucketPourLeStateTerraform\""
  echo "SSH_PUBLIC_KEY_PATH=\"${HOME}/.ssh/id_rsa.pub\""
  echo "REMOTE_DIRECTORY=\"/home/ubuntu/stable-diffusion-webui/log/images/\""
  echo "LOCAL_DIRECTORY=\"$HOME/sd_output/\""
  echo "REMOVE_SOURCE_FILES=true"
  echo "ONLY_ZIP_FILES=true"
  echo
  echo "# Ports"
  echo "SSH_TUNNEL_PORT=8888"
  echo "LOCAL_FORWARDING_PORT=7860"
  echo "--------------------------------------------------------"
  echo
  echo "Ajustez les valeurs en fonction de vos besoins."
  return 1
fi

# Variables globales
set -a
source .env
set +a

AWS_DEFAULT_REGION=$(grep 'region' terraform.tfvars | awk -F'=' '{print $2}' | tr -d ' "')
echo "Region par défaut : $AWS_DEFAULT_REGION"

# Vérification de la présence d'awscli et installation si absent
if ! command -v aws &> /dev/null; then
    echo "awscli n'est pas installé. Installation en cours..."
    pip install awscli
fi

# Vérification et création du bucket S3 pour le stockage de l'état Terraform
echo "Vérification du bucket S3 pour l'état de Terraform..."
aws s3api head-bucket --bucket ${STATE_BUCKET_NAME} || aws s3 mb s3://${STATE_BUCKET_NAME}
aws s3api put-bucket-versioning --bucket ${STATE_BUCKET_NAME} --versioning-configuration Status=Enabled

# Initialisation de Terraform
echo "Initialisation de Terraform..."
terraform init -input=false -backend-config="bucket=${STATE_BUCKET_NAME}" -backend-config="region=${AWS_DEFAULT_REGION}" -backend-config="key=terraform.tfstate"

# Fonction pour récupérer l'ID de l'instance EC2
function get_instance_id {
  aws ssm get-parameter --name "/ec2/sd_instance_id" --query "Parameter.Value" --output text
}

# Fonction pour envoyer une clé publique SSH à l'instance EC2
function send_ssh_public_key {
  local INSTANCE_ID=$(get_instance_id)
  aws ec2-instance-connect send-ssh-public-key --instance-id $INSTANCE_ID --instance-os-user ubuntu --availability-zone ${AWS_DEFAULT_REGION}a --ssh-public-key file://${SSH_PUBLIC_KEY_PATH}
}


# Fonction pour gérer les actions sur l'instance EC2
function manage_ec2_instance {
  local ACTION="$1"
  local INSTANCE_ID=$(get_instance_id)

  case $ACTION in
    start)
      aws ec2 start-instances --instance-ids $INSTANCE_ID
      ;;
    stop)
      aws ec2 stop-instances --instance-ids $INSTANCE_ID
      ;;
    terminate)
      aws ec2 terminate-instances --instance-ids $INSTANCE_ID
      ;;
    backup)
      local NAME_SUFFIX="$2"
      local AMI_NAME="snaplocal-$(date +%Y%m%d%H%M%S)-${NAME_SUFFIX}"
      aws ec2 create-image --instance-id $INSTANCE_ID --name $AMI_NAME --description "Backup de l'instance EC2 depuis le script local" --no-reboot
      echo "Backup créé avec succès avec le nom $AMI_NAME"
      ;;
    status)
      local STATUS=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[0].Instances[0].State.Name" --output text)
      echo "Statut de l'instance EC2 ($INSTANCE_ID) : $STATUS"
      ;;
    *)
      echo "Action non reconnue: $ACTION"
      exit 1
      ;;
  esac
}

# Fonction pour récupérer la zone de disponibilité de l'instance EC2
function get_availability_zone {
  local INSTANCE_ID=$(get_instance_id)
  aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[0].Instances[0].Placement.AvailabilityZone" --output text
}


# Fonction pour se connecter à l'instance EC2 via SSH (avec ou sans tunneling)
function ssh_connect {
  local TUNNEL="$1"
  local INSTANCE_ID=$(get_instance_id)

  if [ "$TUNNEL" == "tunnel" ]; then
    aws ec2-instance-connect ssh --instance-id $INSTANCE_ID --os-user ubuntu --local-forwarding 7860:127.0.0.1:7860 --connection-type eice
  else
    aws ec2-instance-connect ssh --instance-id $INSTANCE_ID --connection-type eice --os-user ubuntu
  fi
}

# Fonctions pour la gestion de Terraform
function terraform_plan {
  terraform validate
  terraform plan -out=tfplan -input=false
}

function terraform_apply {
  terraform apply -input=false tfplan
}

# Fonction pour détruire les ressources Terraform
function terraform_destroy {
  echo "Êtes-vous sûr de vouloir détruire toutes les ressources Terraform ? [O/n]"
  read -r confirmation
  if [[ "$confirmation" == "O" || "$confirmation" == "o" ]]; then
    terraform destroy -auto-approve
  else
    echo "Destruction annulée."
  fi
}

# Fonction pour envoyer une clé publique SSH à l'instance EC2
function update_spot_price {
  local AVAILABILITY_ZONE=$(get_availability_zone)
  local REGION=$(grep 'region' terraform.tfvars | awk -F'=' '{print $2}' | tr -d ' "')
  local INSTANCE_TYPE=$(grep 'instance_type' terraform.tfvars | awk -F'=' '{print $2}' | tr -d ' "')

  if [ -n "$AVAILABILITY_ZONE" ] && [ "$AVAILABILITY_ZONE" != "None" ]; then
    echo "Utilisation de la zone de disponibilité $AVAILABILITY_ZONE pour obtenir le prix spot."
    local NEW_PRICE=$(aws ec2 describe-spot-price-history --instance-types "${INSTANCE_TYPE}" --product-descriptions "Linux/UNIX" --availability-zone "${AVAILABILITY_ZONE}" --query "SpotPriceHistory[0].SpotPrice" --output text)
  else
    echo "Utilisation de la région $REGION pour obtenir le prix spot."
    local NEW_PRICE=$(aws ec2 describe-spot-price-history --instance-types "${INSTANCE_TYPE}" --product-descriptions "Linux/UNIX" --region "${REGION}" --query "SpotPriceHistory[0].SpotPrice" --output text)
  fi

  echo "Nouveau prix récupéré : $NEW_PRICE"

  if [ -n "$NEW_PRICE" ]; then
    sed -i "s/\"${INSTANCE_TYPE}\"[[:space:]]*=[[:space:]]*\"[0-9.]*\"/\"${INSTANCE_TYPE}\" = \"${NEW_PRICE}\"/g" terraform.tfvars
    echo "Mise à jour du prix pour $INSTANCE_TYPE à $NEW_PRICE dans terraform.tfvars."
  else
    echo "Impossible de récupérer le nouveau prix pour $INSTANCE_TYPE."
  fi
}





# Fonction pour établir un tunnel SSH et envoyer une clé publique
function send_key_and_establish_tunnel {
  local INSTANCE_ID=$(get_instance_id)
  local AVAILABILITY_ZONE=$(get_availability_zone)

  # Envoyer la clé publique SSH
  aws ec2-instance-connect send-ssh-public-key \
      --instance-id $INSTANCE_ID \
      --instance-os-user ubuntu \
      --availability-zone $AVAILABILITY_ZONE \
      --ssh-public-key file://${SSH_PUBLIC_KEY_PATH}

  # Établir un tunnel SSH
  aws ec2-instance-connect open-tunnel \
      --instance-id $INSTANCE_ID \
      --local-port $SSH_TUNNEL_PORT
}


# Fonction pour télécharger des fichiers de l'instance EC2
function download_from_instance {
  # Vérifier et créer le répertoire de sortie si nécessaire
  mkdir -p ${LOCAL_DIRECTORY}
  
  # Déterminer si l'option --remove-source-files doit être utilisée
  REMOVE_OPTION=""
  if [ "$REMOVE_SOURCE_FILES" = true ]; then
    REMOVE_OPTION="--remove-source-files"
  fi
  
  # Filtrer pour les fichiers .zip si nécessaire
  FILE_FILTER=""
  if [ "$ONLY_ZIP_FILES" = true ]; then
    FILE_FILTER="*.zip"
  fi
  
  # Utiliser rsync pour synchroniser les fichiers
  rsync -avz ${REMOVE_OPTION} -e "ssh -p $SSH_TUNNEL_PORT -i ~/.ssh/id_rsa" ubuntu@localhost:${REMOTE_DIRECTORY}${FILE_FILTER} ${LOCAL_DIRECTORY}
  
  # Supprimer le répertoire distant si ONLY_ZIP_FILES est activé
  if [ "$ONLY_ZIP_FILES" = true ]; then
    ssh -p $SSH_TUNNEL_PORT -i ~/.ssh/id_rsa ubuntu@localhost "rm -rf ${REMOTE_DIRECTORY}"
  fi
}

# Affichage des commandes disponibles
function display_help {
  echo "Commandes disponibles :"
  echo "  terraform_plan                - Valide et planifie les modifications Terraform."
  echo "  terraform_apply               - Applique les modifications Terraform."
  echo "  terraform_destroy             - Détruit toutes les ressources Terraform après confirmation."
  echo "  manage_ec2_instance [action]  - Gère l'instance EC2 (actions possibles: start, status, stop, terminate, backup)."
  echo "  update_spot_price             - Met à jour le prix spot pour le type d'instance spécifié dans terraform.tfvars."
  echo "  send_ssh_public_key           - Envoie la clé SSH publique à l'instance."
  echo "  ssh_connect [option]          - Se connecte à l'instance via SSH (option: tunnel pour une connexion avec tunneling)."
  echo "  send_key_and_establish_tunnel - Établit un tunnel et envoie la clé SSH publique à l'instance."
  echo "  download_from_instance        - Télécharge des fichiers de l'instance EC2 vers le répertoire ~/sd_output."
}
# Exporter les fonctions pour une utilisation ultérieure
export -f terraform_plan
export -f terraform_apply
export -f terraform_destroy
export -f manage_ec2_instance
export -f update_spot_price
export -f send_ssh_public_key
export -f ssh_connect
export -f send_key_and_establish_tunnel
export -f download_from_instance
export -f display_help

echo "L'initialisation est terminée."
display_help
