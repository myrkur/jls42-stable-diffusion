Déploiement de Stable diffusion sur EC2 AWS  

[[_TOC_]]

# But du Projet

Ce projet permet de déployer facilement "Stable Diffusion XL avec Controlnet" dans le cloud AWS sur une EC2.  
Il automatise la création et la configuration de l'infrastructure nécessaire pour exécuter "Stable Diffusion XL avec Controlnet".  
L'installation de Stable Diffusion est réalisée manuellement une fois l'EC2 disponible (avec un guide).  
Pour limiter les coûts EC2 une instance spot de type g4dn.xlarge est utilisée (1 GPU NVIDIA T4 16 GO GDDR6, 4 vCPU Intel Cascade Lake, 16 GO RAM).  
Le deploiement de Stable Diffusion est effectué avec l'installeur [AUTOMATIC1111](https://github.com/AUTOMATIC1111/stable-diffusion-webui).  
Plus d'information sur le type d'instance utilisé :
- [Instances G4 d'Amazon EC2](https://aws.amazon.com/fr/ec2/instance-types/g4/)  
- [NVIDIA T4](https://www.nvidia.com/fr-fr/data-center/tesla-t4/) 


# Pourquoi ce projet ?
Cela faisait un moment que j'avais envie de tester Stable Diffusion.
J'ai tenté l'expérience sur mon pc qui à plus de dix ans, la génération d'une image 512x512 mettait au moins 15mn. N'ayant pas envie d'investir dans un nouveau PC, la solution cloud AWS me permet d'y arriver à "moindre" coût.  
Je suis ultra débutant en Stable Diffusion.  
Du coup après avoir fait le boulot d'industrialisation pour moi, je me suis dis que j'allais le partager, alors voilà :)

# Attention - Point de vigilence
**Assurez vous de comprendre le code avant de l'utiliser.**  
**Utiliser AWS est payant**. Renseignez-vous si vous n'êtes pas informés.  
Ne laissez pas votre instance tourner toute la journée sans vous en servir sinon ça va vous coûter de l'argent pour rien: il faut la stopper. Dans ce cas, il n'y aura plus de coût d'instance, mais il restera tout de même, à minima des coûts de stockage (EBS). [Tarification Amazon EBS](https://aws.amazon.com/fr/ebs/pricing/)  
Et quand vous ne comptez plus du tout l'utiliser, lancer le job de destruction des ressources.  
Vous pouvez aussi lancer un backup (création d'une AMI) avant la destruction, pour par exemple garder une sauvegarde de votre installation pour une utilisation ultérieure. Ensuite dans le fichier `terraform.tfvars` vous pourrez indiquer à la place de la version d'AMI présente, l'AMI nouvellement générée suite au lancement du backup. Ainsi, après redéploiement, vous retrouverez vos données comme avant la destruction.  
En cas de suppression totale de l'infrastructre via le script de destruction, les backups ne seront pas inclus, il faudra les supprimer à la main depuis la console.  
Il y a aussi un coût associé au stockage des backups, le coût S3 de stockage est utilisé : [Tarification Amazon S3](https://aws.amazon.com/fr/s3/pricing/)
Et si vous n'êtes pas déjà familiarisé avec AWS veuillez lire ça : [Bonnes pratiques de sécurité dans IAM](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/best-practices.html#enable-mfa-for-privileged-users)
Côté instances, les spots sont utilisés. Vous avez le choix entre Persistent request (Demande persistante) qui est par défaut et One-time (Unique).
Request type (Type de demande) : le type de demande d'instance spot que vous choisissez détermine ce qui se passe si votre instance spot est interrompue.
- One-time (Unique) : Amazon EC2 effectue une demande unique pour votre instance Spot. Si votre instance Spot est interrompue, la demande n'est pas soumise à nouveau.
- Persistent request (Demande persistante) : Amazon EC2 place une demande persistante pour votre instance Spot. Si votre instance spot est interrompue, la demande est soumise à nouveau afin de réapprovisionner l'instance spot résiliée.
Plus d'informations sur les spots :
- [Tarification des instances Spot Amazon EC2](https://aws.amazon.com/fr/ec2/spot/pricing/)
- [Utiliser instances Spot](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/spot-requests.html)  

**Vérifiez régulièrement vos coûts AWS** : [Tableau de bord de facturation AWS](https://us-east-1.console.aws.amazon.com/billing/)  

# Architecture

![Architecture du Projet](img/architecture.drawio.png)

### Configuration EC2 Instance Connect Endpoint

Dans le cadre de ce projet, EC2 Instance Connect Endpoint est utilisé pour établir des connexions sécurisées aux instances EC2. Cette méthode offre une approche moderne pour accéder aux instances, en remplaçant les méthodes traditionnelles. 
Un élément crucial de cette configuration est la définition des adresses IP autorisées à se connecter à l'instance. Cette configuration est gérée dans le fichier `.vars`. Plutôt que de demander manuellement à l'utilisateur de fournir son adresse IP, le processus est automatisé.   
Lors de l'exécution du script `generate_env.sh`, l'adresse IP publique actuelle de l'utilisateur est détectée et récupérée automatiquement. Cette IP est ensuite insérée dans le fichier `.vars` comme l'adresse IP autorisée par défaut. Cela garantit que l'utilisateur peut se connecter à l'instance EC2 depuis son emplacement actuel sans avoir à effectuer de configurations manuelles supplémentaires. Si votre adresse IP internet change, pensez à modifier la valeur dans le fichier `terraform.tfvars` et de relancer le déploiement (mise à jour) infrastructure pour prendre en compte cette nouvelle ip.   

Cette automatisation non seulement simplifie le processus de configuration pour l'utilisateur, mais garantit également que l'adresse IP est correctement définie, réduisant ainsi les risques d'erreurs manuelles. En combinant EC2 Instance Connect avec cette automatisation, une connexion sécurisée et efficace aux instances EC2 est assurée.

# Pré-requis

- **Compte AWS** : Avoir un compte AWS. [Documentation AWS](https://repost.aws/fr/knowledge-center/create-and-activate-aws-account)
- **Activer l'utilisation des spots g4dn**: [AWS Quotas](https://eu-west-1.console.aws.amazon.com/servicequotas/home/services/ec2/quotas) vérifier que la région est la même que celle configurée dans votre `terraform.tfvars` (ici Irlande) et lancer la recherche sur : **All G and VT Spot Instance Requests**. Ensuite **demander une augmentation de quota** et entrer la valeur **4** dans la boîte de saisie et cliquer **Demander** (4 c'est le nombre de vCPU utilisés par l'instance g4dn.xlarge).
- **Avoir un shell linux** : Les commandes d'installation d'infrastructure sont codées et testée pour Linux sous bash.
- **Utilisateur AWS IAM** :
  - **Pour le déploiement local** : Créer un utilisateur IAM avec la politique `arn:aws:iam::aws:policy/AdministratorAccess`. (ne pas utiliser le compte "root")
  - **Pour un déploiement via gitlab CI** : Configurer un utilisateur avec les droits appropriés (admin ou plus limités, à vous de voir)
- **AWS CLI** : Assurez-vous d'avoir le client AWS installé et configuré. [Documentation AWS](https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/getting-started-install.html)
    (Si vous avez le paquet 'pip' le client AWS sera installé automatique si jamais vous ne l'avez pas. La configuration ne sera cependant pas lancée.)
- **Terraform**: Assurez-vous que Terraform soit installé. Le plus simple pour gérer Terraform est d'utiliser tfswitch [tfswitch](https://tfswitch.warrensbox.com/Install/), sinon, si tfswitch ne vous convient pas, voir la documentation officielle [Terraform installation](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- **Bucket**: Il vous faudra un bucket pour mettre les états de Terraform (state) 
```bash
aws s3 mb s3://monNomDeBucket
aws s3api put-bucket-versioning --bucket "monNomDeBucket" --versioning-configuration Status=Enabled
```

# Installation
Ici nous verrons le déploiement local (hors ci gitlab)  
## Configuration AWS
### Configurer le client AWS :  
- Une fois votre utilisateur créé et les droits d'admin, créer une "access key" sur votre utilisateur AWS  
voir la doc : [Create Accesskey](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey)
```bash
## Repondre aux question du aws configure (en gros donner les informations de clef acces key id et secret key id et region)
aws configure
# On vérifie le bon fonctionnement, cette commande ne doit pas retourner d'erreur.
aws s3 ls
```
## Configuration de base
### Récupération du code de ce dépôt et génération des fichiers de configuration :
- Clonage de ce dépôt  
```bash
git clone git@gitlab.com:jls42/jls42-stable-diffusion.git # ou bien git clone https://gitlab.com/jls42/jls42-stable-diffusion.git
cd jls42-stable-diffusion
## Si vous n'avez pas de clef SSH en local il va falloir en créer une, par defaut il va dans /home/VOTRE_UTILISATEUR/.ssh/id_rsa
ssh-keygen
```
### Exécutez le script generate_env.sh :

#### Initialisation avec `generate_env.sh` - Explications

Avant de déployer votre infrastructure avec Terraform, il est crucial de configurer correctement votre environnement. Le script `generate_env.sh` a été conçu pour faciliter cette étape.

##### **Fonctionnement du script**:
1. **Vérification de l'existence du fichier `.env`**: Si un fichier `.env` existe déjà, le script vous avertira afin d'éviter toute surcharge accidentelle.
2. **Collecte d'informations locales**: Une série de questions est posée pour configurer les variables essentielles. Chaque question propose une valeur par défaut, mais vous êtes libre d'entrer des valeurs personnalisées.
3. **Détermination de l'IP actuelle**: Le script détecte et utilise votre IP publique actuelle comme valeur par défaut pour les IPs autorisées, offrant ainsi une couche de sécurité.
4. **Recherche de la dernière AMI**: Il recherche automatiquement la dernière version de l'AMI "Deep Learning AMI GPU PyTorch". En cas d'erreur ou d'absence de l'AMI, une valeur par défaut est utilisée.
5. **Génération de `terraform.tfvars`**: Ce fichier est crucial pour Terraform, contenant des variables comme la région AWS, les CIDR du VPC, l'ID de l'AMI, et d'autres.
6. **Création du fichier `.env`**: Contient d'autres variables d'environnement essentielles pour le projet.
7. **Mise à jour du `.gitlab-ci.yml`**: Si présent, le script met à jour certaines variables pour assurer la cohérence de la configuration.

##### **Pourquoi est-ce essentiel ?**
En utilisant `generate_env.sh`, vous garantissez une initialisation fluide et correcte de votre environnement Terraform. Cela minimise les risques d'erreurs lors du 1er déploiement de l'infrastructure et assure une configuration cohérente pour démarrer.

```bash
./generate_env.sh
```
Ce script générera les fichiers nécessaires comme `terraform/.env` et `terraform/terraform.tfvars`.  

Maintenant, deux possibilités pour déployer l'infrastructure, soit via votre poste, soit via la CI dans gitlab.com. Allez au chapitre associé.  

## Déploiement infrastucture depuis la ci gitlab.com
Supprimer le dossier .git
```bash
rm -rf .git
```
Vérifiez ensuite le fichier `terraform/.env` et changer le prix si nécessaire pour l'instance g4dn.xlarge.  
Rendez vous sur cette page [Tarification des instances Spot Amazon EC2](https://aws.amazon.com/fr/ec2/spot/pricing/), le prix du fichier .env ne doit pas être inférieur.  
Choisissez la Région et les intances de type GPU puis regardez le prix de la g4dn.xlarge.  

Créez un projet sur gitlab.com puis poussez le code dedans en suivant les instruction de gitlab.com.  

Il faudra ensuite configurer votre projet pour qu'il accède à votre compte AWS, deux choix possibles : 
- [Deploy to AWS from GitLab CI/CD](https://docs.gitlab.com/ee/ci/cloud_deployment/) (NE PARTAGER PAS VOS CODES, NE LES METTEZ PAS DANS LES FICHIERS GIT)
- [Configure OpenID Connect in AWS to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/aws/index.html) Si vous partez sur cette solution, il faudra rajouter le job d'authentification dans le script de pipeline `.gitlab-ci.yml`

Dans la section build -> pipeline   
Lancer le plan puis l'apply.  
L'infrastructure est alors déployée, vous pouvez à présent installer Stable diffusion dessus, [Voir la section sur le déploiement de Stable Diffusion](#déploiement-de-stable-diffusion).  


## Déploiement infrastucture depuis son poste local

### Préparation pour le Déploiement de l'Infrastructure : 

- Allez dans le dossier `terraform`.
- Sourcez le script `export.sh` pour définir les variables d'environnement nécessaires :
```bash
cd terraform
source export.sh 

```
Une fois lancée voici ce que ça donne. Il ne doit pas y avoir d'erreur : 

```bash
[...]
L'initialisation est terminée.
Commandes disponibles :
terraform_plan                - Valide et planifie les modifications Terraform.
terraform_apply               - Applique les modifications Terraform.
terraform_destroy             - Supprime toutes les ressources Terraform.
manage_ec2_instance [action]  - Gère l'instance EC2 (actions possibles: start, status, stop, terminate, backup).
update_spot_price             - Met à jour le prix spot pour le type d'instance spécifié dans terraform.tfvars.
send_ssh_public_key           - Envoie la clé SSH publique à l'instance.
ssh_connect [option]          - Se connecte à l'instance via SSH (option: tunnel pour une connexion avec tunneling).
establish_tunnel_and_send_key - Établit un tunnel et envoie la clé SSH publique à l'instance.
download_from_instance        - Télécharge des fichiers de l'instance EC2 vers le répertoire ~/sd_output.
```

### Déploiement infra :  
- On lance terraform plan via cette commande (avant on mets à jour les prix spot)
- Et après on lance le 'plan' afin de voir ce qu'il va nous créer dans AWS 
``` bash
update_spot_price
terraform_plan 
```
- Ensuite on déploie l'infrastructure via 'apply', ça va juste appliquer ce qu'on a vu dans le 'plan'
```bash
terraform_apply     
```

L'installation infrastructre est à présent terminée.

## Déploiement de Stable Diffusion
### Connexion à l'instance et déploiement via AUTOMATIC1111

Pour déployer Stable Diffusion on va se connecter à l'instance et réaliser le déploiement de Stable diffusion avec AUTOMATIC1111.  
Allez dans le dossier Terraform du répertoire jls42-stable-diffusion (si vous n'y êtes pas déjà).  
- Allez dans le dossier `terraform`.
- Sourcez le script `export.sh` pour définir les variables d'environnement nécessaires :
```bash
cd terraform
source export.sh 
```

- Lancement de la commande ssh_connect (la connexion dure 1h max, il faut relancer au bout d'une heure)
```bash
send_ssh_public_key
ssh_connect 
```
On vérifie l'état du cloud init (installation des composants nécessaires à la suite, si c'est marqué running, c'est que c'est en cours).   
```bash

sudo cloud-init status
## Retour de la commande : 
# status: running
```

Pour éviter d'attendre devant l'écran pendant le "running", on va lancer une commande qui va attendre que ce soit prêt avant de lancer l'installation.  
Cette commande c'est l'utilisation du script webui.sh Il a été téléchargé lors du déploiement de l'instance, voir le script : `terraform/scripts/user_data.sh`  

```bash
# on lance cette commande pour attendre la fin du cloud-init avant de lancer le déploiement de stable diffusion
while [[ "$(sudo cloud-init status | grep -o 'status: [a-z]*' | cut -d ' ' -f 2)" != "done" ]]; do sleep 10; done && ./webui.sh --xformers
```

Voici un bout du retour de la commande :  
```bash
################################################################
Install script for stable-diffusion + Web UI
Tested on Debian 11 (Bullseye)
################################################################

################################################################
Running on ubuntu user
################################################################

################################################################
Create and activate python venv
################################################################

################################################################
Launching launch.py...
################################################################
Using TCMalloc: libtcmalloc_minimal.so.4
[...]
Running on local URL:  http://127.0.0.1:7860
[...]
Model loaded in 55.7s (load weights from disk: 3.3s, create model: 0.7s, apply weights to model: 51.0s, calculate empty prompt: 0.5s).
```

Ce dernier message indique que le programme est démarré, on peut alors l'arrêter pour déployer les modèles de base SDXL puis Controlnet et ces modèles pour SDXL.  
```bash
# appuyer simultanément sur les touches du clavier CTRL et C
ctrl +c
```

### Téléchargement de modèles et extensions supplémentaires :  
Ici on va déployer controlnet et des modèles SDXL + des modèles controlnet compatibles SDXL (il faut bien avoir quitté la webui via le ctrl+c)  
Il y a un modèle déployé avec l'installation mais c'est v1-5-pruned-emaonly.safetensors.    
Copier/Coller l'ensemble des commandes suivantes dans le terminal de la machine distante (ubuntu) puis appuyer sur la touche **Enrée**.   

```bash
#!/bin/bash

# déploiement du modèle sdxl base et son refiner
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0/resolve/main/sd_xl_base_1.0.safetensors" -d stable-diffusion-webui/models/Stable-diffusion -o sd_xl_base_1.0.safetensors
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "https://huggingface.co/stabilityai/stable-diffusion-xl-refiner-1.0/resolve/main/sd_xl_refiner_1.0.safetensors" -d stable-diffusion-webui/models/Stable-diffusion -o sd_xl_refiner_1.0.safetensors

# installation de l'extension controlnet
git clone https://github.com/Mikubill/sd-webui-controlnet stable-diffusion-webui/extensions/sd-webui-controlnet

# deploiement des modèles XL pour controlnet
BASE_URL="https://huggingface.co/lllyasviel/sd_control_collection/resolve/main/"

MODELS=(
    "diffusers_xl_canny_full.safetensors"
    "diffusers_xl_depth_full.safetensors"
    "ioclab_sd15_recolor.safetensors"
    "ip-adapter_sd15_plus.pth"
    "ip-adapter_sd15.pth"
    "ip-adapter_xl.pth"
    "kohya_controllllite_xl_blur.safetensors"
    "kohya_controllllite_xl_blur_anime.safetensors"
    "kohya_controllllite_xl_canny.safetensors"
    "kohya_controllllite_xl_canny_anime.safetensors"
    "kohya_controllllite_xl_blur_anime_beta.safetensors"
    "kohya_controllllite_xl_depth.safetensors"
    "kohya_controllllite_xl_depth_anime.safetensors"
    "kohya_controllllite_xl_openpose_anime.safetensors"
    "kohya_controllllite_xl_openpose_anime_v2.safetensors"
    "kohya_controllllite_xl_scribble_anime.safetensors"
    "sai_xl_canny_256lora.safetensors"
    "sai_xl_depth_256lora.safetensors"
    "sai_xl_recolor_256lora.safetensors"
    "sai_xl_sketch_256lora.safetensors"
    "sargezt_xl_depth.safetensors"
    "sargezt_xl_depth_faid_vidit.safetensors"
    "sargezt_xl_depth_zeed.safetensors"
    "sargezt_xl_softedge.safetensors"
    "t2i-adapter_diffusers_xl_canny.safetensors"
    "t2i-adapter_diffusers_xl_depth_midas.safetensors"
    "t2i-adapter_diffusers_xl_depth_zoe.safetensors"
    "t2i-adapter_diffusers_xl_lineart.safetensors"
    "t2i-adapter_diffusers_xl_openpose.safetensors"
    "t2i-adapter_xl_canny.safetensors"
    "t2i-adapter_xl_openpose.safetensors"
    "t2i-adapter_xl_sketch.safetensors"
    "thibaud_xl_openpose.safetensors"
    "thibaud_xl_openpose_256lora.safetensors"
)

for model in "${MODELS[@]}"; do
    aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "${BASE_URL}${model}" -d stable-diffusion-webui/extensions/sd-webui-controlnet/models -o "${model}"
done

echo "Téléchargement terminé!"
```

### Lancement de la Web UI

Ici on va lancer la webui en tâche de fond, en cas de perte de connexion SSH ça ne tuera pas la webui (coupure toutes les heures)
```bash
nohup ./webui.sh --xformers > output.log 2>&1 & tail -f output.log
```
Quand c'est fini, voici ce qui est marqué :
```bash
[...]
Creating model from config: /home/ubuntu/stable-diffusion-webui/repositories/generative-models/configs/inference/sd_xl_base.yaml
Applying attention optimization: xformers... done.
Model loaded in 57.1s (load weights from disk: 4.2s, create model: 0.7s, apply weights to model: 51.1s, load textual inversion embeddings: 0.2s, calculate empty prompt: 0.8s).
```

L'installation est maintenant terminée, vous pouvez jouer avec l'outil.  
Pour couper Stable Diffusion, ctrl+c pour quitter tail puis la commande fg pour reprendre la main sur le WebUI puis ctrl+c pour couper la WebUI.  

## Accès à la WebUI de Stable Diffusion sur l'instance distante
### Mise en place du tunnel pour accéder à l'instance :  
L'instance n'est pas directement accessible par internet, il faut ouvrir un tunnel, tout est automatisé.  
- Ouvrez un autre terminal (ne couper pas WebUI) et allez dans le dossier jls42-stable-diffusion téléchargé au début puis dans terraform et lancez l'export.sh (webui doit être lancée sur l'ec2)
```bash
cd jls42-stable-diffusion/terraform 
source export.sh
## ici on va faire un tunnel entre l'instance distante et le pc local sur le port 7860
ssh_connect tunnel
```
Maintenant vous pouvez taper dans le navigateur (firefox, chrome, etc) : http://127.0.0.1:7860/ et vous devriez voir apparaître l'interface de stable diffusion.  

### Test de Stable Diffusion :  
En haut à gauche, dans la liste des check point (Stable Diffusion checkpoint) on choisi : sd_xl_base_1.0.safetensors   
Vous pouvez aussi le coupler avec le "Refiner", mais la génération sera beaucoup plus lente, je ne conseille pas sur le premier test. Malgré tout, si vous voulez tester, dans le menu Refiner (vers le milieu) on sélectionne : sd_xl_refiner_1.0.safetensors

Pour les réglage de départ tentez : 
- Sampling steps : 30
- Width : 1024
- Height : 1024 
- Batch count : 1

Puis faites un test de prompt, par exemple :  "panda, mignon et adorable, dessin animé, fantastique, surréalisme, super mignon"  
Voici le résultat avec le prompt du panda :  
![Rendu du prompt](img/refiner_sdxl.png)

Le premier lancement est plus lent car il va encore télécharger des dépendances.  

Puis tentez ce prompt : "Femme, moderne, 2023, cheveux tendance, portrait, haute résolution, détails précis, appareil photo reflex, objectif professionnel, netteté, mise au point parfaite, lumière naturelle, réalisme, texture de peau parfaite, expression naturelle, profondeur de champ réduite, arrière-plan flou, qualité studio, style contemporain"

Cela vous donne déjà un aperçu du modèle de base.  
Si vous trouvez la génération lente, n'abandonez pas là, allez à la section suivante :)

**Voir plus bas l'installation d' modèle supplémentaire, ne pas rester sur ce test avec le modèle de base.**  

### Ajouter des modèles SDXL supplémentaires   
Les premiers tests étaient réalisés avec le modèle de base+ refiner, mais il existe plein de modèles sur https://huggingface.co/ ou https://civitai.com/ (filtrer par checkpoint).  
Il faut télécharger celui qu'on veut puis le placer dans stable-diffusion-webui/extensions/sd-webui-controlnet
#### Exemple
Pour l'exemple, j'ai testé avec ce modèle là : [DynaVision XL - All-in-one stylized 3D SFW and NSFW output](https://civitai.com/models/122606/dynavision-xl-all-in-one-stylized-3d-sfw-and-nsfw-output-no-refiner-needed)  
***IL NE FAUT PAS L'UTILISER AVEC LE REFINER, ainsi ne pas le spécifier dans la webui*** (c'est marqué sur le lien ci-dessus) 

### Déploiement du modèle, d'abord on arrête la webui !
Pour stopper la webui appuyer sur ctrl et c simultanément plusieurs fois jusqu'à reprendre la main sur le shell.  
```bash
# Une fois la Web UI coupée, on lance le téléchargement du modèle DynaVision XL
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M "https://civitai.com/api/download/models/198962" -d stable-diffusion-webui/models/Stable-diffusion -o dynavisionXLAllInOneStylized_release0557Bakedvae.safetensors
```

on relance webui et on choisit en haut à gauche le checkpoint associé (dynavisionXLAllInOneStylized_release0534bakedvae.safetensors).   
Il est long à charger au premier lancement mais aux suivants c'est bon.  
```bash
nohup ./webui.sh --xformers > output.log 2>&1 & tail -f output.log
```

Ensuite on refait les tests de prompts joués plus haut pour comparer.   

Pour les réglages de départ, tentez : 
- Sampling steps : 30
- Width : 1024
- Height : 1024 
- Batch count : 4

**Oui batch count 4 pour avoir 4 images pour tester ce paramètre**.  
Voici le résultat avec le prompt du panda :  
![Rendu du prompt](img/dynavisionxl.png)

Sinon pour utiliser Controlnet je vous invite à regarder sur youtube des vidéos en français il y en a quelques unes récentes et sympas.  

Voilà vous avez la base pour tester stable diffusion et faire vos premiers pas. Enjoy !

## Commandes disponibles
Après avoir sourcé export.sh vous avez ces commandes disponibles : 
- Déploiement infra et mise à jour infra  
    - terraform_plan                - Valide et planifie les modifications Terraform.
    - terraform_apply               - Applique les modifications Terraform.
    - terraform_destroy             - Remet à zéro l'infrastructure, supprime toutes les ressources
- Gestion de l'instance EC2  
    - manage_ec2_instance [action]  - Gère l'instance EC2 (actions possibles: start, status, stop, terminate, backup).
- Mise à jour du prix des instances spot  
    - update_spot_price             - Met à jour le prix spot pour le type d'instance spécifié dans terraform.tfvars.
- Gestion des interaction avec l'instance  
    - send_ssh_public_key           - Envoie la clé SSH publique à l'instance.
    - ssh_connect [option]          - Se connecte à l'instance via SSH (option: tunnel pour une connexion avec tunneling).
    - send_key_and_establish_tunnel - Envoie la clé SSH publique à l'instance puis établit un tunnel
    - download_from_instance        - Télécharge des fichiers sauvegardés de l'instance EC2 vers le répertoire local du post ~/sd_output. (sauf si configuré ailleurs)

# Détails du Code

- **Dossier Terraform** :
  - `variables.tf`: Définit les variables utilisées dans les configurations Terraform. Ces variables peuvent avoir des valeurs par défaut et peuvent être surchargées par le fichier `terraform.tfvars`.
  - `outputs.tf`: Spécifie les sorties Terraform, comme l'ID de l'instance EC2.
  - `export.sh`: Charge les variables d'environnement à partir du fichier `.env`, vérifie la présence des dépendances et fournit des alias pour les commandes Terraform et des commandes d'interaction avec la machine distante.
  - `.env`: Contient des variables d'environnement nécessaires pour le déploiement.
  - `lambda_autostop.tf`: Configure une fonction AWS Lambda pour arrêter automatiquement les instances EC2 la nuit.
  - `terraform.tfvars`: Contient les valeurs pour surcharger les variables définies dans `variables.tf`.
  - `igw.tf`: Configure une passerelle Internet pour le VPC.
  - `backend.tf`: Configure le backend de Terraform pour stocker l'état de Terraform dans un bucket S3.
  - `lambdas/autostop.py`: Code source Python de la fonction Lambda pour arrêter automatiquement les instances EC2.
  - `ssm.tf`: Configure le AWS Systems Manager pour la gestion des instances EC2.
  - `providers.tf`: Configure les fournisseurs Terraform utilisés.
  - `subnets.tf`: Définit les sous-réseaux du VPC. Ici on en a 3, des fois il peut manquer de capacité sur une Zone.
  - `ec2_instance_connect.tf`: Configure EC2 Instance Connect.
  - `budget.tf`: Définit un budget AWS pour surveiller les coûts.
  - `scripts/user_data.sh`: Script exécuté lors de l'initialisation de l'instance EC2.
  - `scripts/manage_ec2.sh`: Script pour gérer les opérations liées aux instances EC2.
  - `scripts/check_variable.sh`: Vérifie si certaines variables d'environnement ou configurations sont correctement définies.
  - `routes.tf`: Définit les routes pour le VPC.
  - `vpc.tf`: Configuration du VPC.
  - `ec2.tf`: Configure l'instance EC2.

- **Autres Fichiers/Dossiers** :
  - `generate_env.sh`: Génère les fichiers de configuration initiaux pour Terraform.
  - `.gitlab-ci.yml`: Configuration pour GitLab CI.

## Zoom sur le fichier `export.sh`

### **Configuration initiale**

#### **Vérification du fichier `.env`**
Le fichier `.env` est essentiel pour le bon fonctionnement du script. Il contient les variables d'environnement nécessaires à diverses opérations. Si ce fichier est absent, le script affichera un message d'erreur et s'arrêtera.

Voici les variables attendues dans le fichier `.env` :
- **STATE_BUCKET_NAME** : Nom du bucket S3 utilisé pour stocker l'état de Terraform.
- **SSH_PUBLIC_KEY_PATH** : Chemin vers la clé publique SSH, généralement située dans `~/.ssh/id_rsa.pub`.
- **REMOTE_DIRECTORY** : Chemin du répertoire distant sur l'instance EC2 d'où les fichiers doivent être téléchargés.
- **LOCAL_DIRECTORY** : Chemin du répertoire local où les fichiers doivent être sauvegardés.
- **REMOVE_SOURCE_FILES** : Si `true`, les fichiers seront supprimés de l'instance EC2 après le téléchargement.
- **ONLY_ZIP_FILES** : Si `true`, seuls les fichiers `.zip` seront téléchargés.
- **SSH_TUNNEL_PORT** : Port utilisé pour établir un tunnel SSH.
- **LOCAL_FORWARDING_PORT** : Port utilisé pour le transfert de port local.

### **Fonctions définies**

#### **get_instance_id**
Cette fonction récupère l'ID de l'instance EC2 à partir d'un paramètre nommé "/ec2/sd_instance_id" dans AWS Systems Manager Parameter Store. Cette ID est essentielle pour de nombreuses opérations.

#### **manage_ec2_instance**
Gère l'instance EC2 pour diverses actions :
- **start** : Démarrage de l'instance EC2.
- **stop** : Arrêt de l'instance EC2.
- **terminate** : Suppression de l'instance EC2.
- **backup** : Création d'une image (AMI) de l'instance EC2 pour la sauvegarde.
- **status** : Affichage de l'état actuel de l'instance EC2.

#### **update_spot_price**
Met à jour le prix spot de l'instance EC2 dans le fichier `terraform.tfvars` en fonction du type d'instance spécifié.

#### **send_ssh_public_key**
Envoie la clé publique SSH spécifiée par `SSH_PUBLIC_KEY_PATH` à l'instance EC2, permettant ainsi des connexions SSH ultérieures.

#### **ssh_connect**
Établit une connexion SSH à l'instance EC2. Avec l'option "tunnel", un tunnel SSH est établi pour le transfert de port.

#### **send_key_and_establish_tunnel**
Combinaison des deux fonctions précédentes : établit un tunnel SSH et envoie la clé SSH publique à l'instance EC2.

#### **download_from_instance**
Télécharge des fichiers depuis l'instance EC2 vers le répertoire local spécifié par `LOCAL_DIRECTORY`. Les options `REMOVE_SOURCE_FILES` et `ONLY_ZIP_FILES` permettent de contrôler quels fichiers sont téléchargés et s'ils doivent être supprimés de l'instance après le téléchargement. Il faut avoir lancé avant dans un terminal la fonction send_key_and_establish_tunnel.  

#### **display_help**
Affiche une aide pour toutes les commandes disponibles dans le script.



### Et aussi

#### Fonction Lambda d'arrêt automatique
La fonction `autostop` est configurée pour arrêter automatiquement les instances EC2 ayant le tag `autostop` avec la valeur `true`. Elle est déclenchée par un événement CloudWatch basé sur l'heure spécifiée par la variable `autostop_time`.
#### Script de démarrage (User Data)
Le script `user_data.sh` est utilisé pour initialiser et configurer l'instance EC2 lors de sa création. Il met à jour les paquets, installe des dépendances nécessaires, et télécharge le script `webui.sh` depuis un dépôt GitHub.

### Configuration du Budget AWS

La gestion proactive des coûts est essentielle pour toute infrastructure AWS. La configuration de votre budget est réalisée grâce à la combinaison de `budget.tf` et des valeurs définies dans `terraform.tfvars`.

#### Fonctionnalité de Budget dans `budget.tf`:
La configuration détaillée dans `budget.tf` vous permet d'établir un budget AWS, de définir des seuils pour les dépenses et de recevoir des notifications lorsque ces seuils sont atteints.

- **Nom du Budget**: Déterminé par `budget_name`.
- **Type de Budget**: Basé sur la valeur `budget_type`.
- **Limite de Budget**: Actuellement fixée à 100 unités monétaires.
- **Périodicité du Budget**: Fixée par `time_unit`.
- **Alertes de Budget**: Se déclenchent en fonction des seuils définis et envoient des notifications aux adresses e-mail listées.

### Configuration correspondante dans `terraform.tfvars`:

- **budget_alert_thresholds**: Liste des seuils (en pourcentage) qui, lorsqu'ils sont dépassés par rapport au budget total, déclenchent des alertes.
- **budget_name**: Identifiant pour le budget AWS.
- **budget_type**: Type du budget (généralement basé sur les coûts).
- **time_unit**: Périodicité du budget (e.g., "MONTHLY").
- **subscriber_email_addresses**: Adresses e-mail pour les notifications du budget.

Grâce à `terraform.tfvars`, vous pouvez personnaliser ces paramètres en fonction de vos besoins spécifiques, garantissant une surveillance et une gestion adaptées de vos dépenses AWS.

#### Gestion de l'instance EC2
Le script `manage_ec2.sh` offre une interface de ligne de commande pour gérer l'instance EC2. Les actions possibles sont : démarrage, arrêt, terminaison, création de sauvegarde, et vérification de l'état.
#### Vérification des variables d'environnement
Avant d'exécuter certaines actions, il est essentiel de vérifier que toutes les variables d'environnement nécessaires sont définies. Le script `check_variable.sh` effectue cette vérification.
#### Configuration Terraform
La configuration Terraform dans le dossier `terraform` permet de déployer et de gérer l'infrastructure AWS nécessaire pour le projet. Elle configure le VPC, les sous-réseaux, l'instance EC2, la fonction Lambda, et d'autres ressources associées.

# Gestion des problèmes 
```bash
╷
│ Error: creating EC2 Instance: SpotMaxPriceTooLow: Your Spot request price of 0.1971 is lower than the minimum required Spot request fulfillment price of 0.1975.
│       status code: 400, request id: 37e31bf9-c94c-4afe-aa20-9960b4c7fb4a
│ 
│   with aws_instance.main,
│   on ec2.tf line 2, in resource "aws_instance" "main":
│    2: resource "aws_instance" "main" {
│ 
╵
```
Lancer la commande `update_spot_price`, si ça ne corrige pas le souci, éditer le fichier `terraform.tfvars` et changer la valeur de spot_max_price pour le type d'instance qui est paramètrée par défaut.  
En gros, mettre la valeur remontée par l'erreur, ici on mettrait `0.1975`.



```bash
╷
│ Error: creating EC2 Instance: InsufficientInstanceCapacity: There is no Spot capacity available that matches your request.
│       status code: 500, request id: 00027970-31de-46ec-babc-a7e94388a481
│ 
│   with aws_instance.main,
│   on ec2.tf line 2, in resource "aws_instance" "main":
│    2: resource "aws_instance" "main" {
│ 
╵

Ou encore 
╷
│ Error: creating EC2 Instance: InsufficientInstanceCapacity: We currently do not have sufficient g4dn.xlarge capacity in the Availability Zone you requested (eu-west-1c). Our system will be working on provisioning additional capacity. You can currently get g4dn.xlarge capacity by not specifying an Availability Zone in your request or choosing eu-west-1a, eu-west-1b.
│       status code: 500, request id: f270c6b5-a299-4d70-943f-1962f7dbe08a
│ 
│   with aws_instance.main,
│   on ec2.tf line 2, in resource "aws_instance" "main":
│    2: resource "aws_instance" "main" {
│ 
╵

```

Il faut alors changer de zone de disponibilité. Dans `variables.tf` changer le paramètre "subnet_index", il peut avoir la valeur 0, 1 ou 2. Et malgré tout si pas ok, il faut alors changer de région.  

# Auteur

Julien LE SAUX  
Email : contact@jls42.org

# Clause de non-responsabilité :
EN UTILISANT CE SCRIPT, VOUS RECONNAISSEZ QUE VOUS LE FAITES À VOS PROPRES RISQUES. L'AUTEUR N'EST PAS RESPONSABLE DES FRAIS ENGENDRÉS, DES DOMMAGES OU DES PERTES RÉSULTANT DE L'UTILISATION DE CE SCRIPT. ASSUREZ-VOUS DE COMPRENDRE LES FRAIS ASSOCIÉS À AWS AVANT D'UTILISER CE SCRIPT.

# Licence

Ce projet est sous licence GPL.
